# MEMO Crypto TP2

## Kerberos 

Principe:

    Le système d’authentification Kerberos est celui utilisé à l’IUT’O. Voici ce que
    nous en dit wikipedia.
    Kerberos est un protocole d’authentification réseau qui repose sur un mécanisme
    de clés secrètes (chiffrement symétrique) et l’utilisation de tickets, et non de
    mots de passe en clair, évitant ainsi le risque d’interception frauduleuse des mots
    de passe des utilisateurs. Créé au Massachusetts Institute of Technology en 1988,
    il porte le nom grec de Cerbère, gardien des Enfers.

### Commandes à retenir

`klist` : voir son ticket, Le ticket joue le rôle
d’une carte d’identité à péremption assez courte, huit heures généralement.

`ssh` (machine du voisin)_ : vous connecter à distance sur la
machine de votre voisin.

`kinit` : afin de valider un ticket Kerberos mais alors vous devez
fournir à la machine distante votre mot de passe lorsque vous n'avez pas utiliser le -K lors de la commande ssh plus haut.

`kdestroy -A` : détruit vos tickets.

`ssh -o PreferredAuthentications=gssapi-with-mic -K (machine)` : **spécifier à votre client SSH que vous ne souhaitez pas utiliser de système d’authentification autre que Kerberos.**

`ssh-keygen -t rsa -f .ssh/id_rsa_test` :  **créer une clef rsa utilisable pour se connecter avec ssH.**

`ssh -o PreferredAuthentications=publickey -i .ssh/id_rsa_test oXXXXX@info22-01` : **Connexion via clef publique**

`scp -o PreferredAuthentications=password .ssh/id_rsa_test.pub \ oXXXXX@info22-01:/home/iut45/Etudiants/oXXXXX/.ssh/authorized_keys` : **Copie de la clef publique dans le fichier authorized_keys de la machine
distante (en forçant l’usage du mot de passe afin de ne pas utiliser
Kerberos)**

`cat ~/.ssh/id_rsa.pub | ssh -o PreferredAuthentications=password \ oXXXXX@info23-01 "umask 077 && mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys"` : 



    Afin d’utiliser cette clef pour se connecter à une machine à distance, il suffit de
    dire à cette machine qu’elle doit accepter une connexion sans mot de passe à
    la condition que l’utilisateur possède la clef privée d’un couple (clef privée,clef
    publique). Et l’élégance du système est là: il suffit de donner à la machine
    distante la clef publique de ce couple. Le protocole est basé sur le fait que seule
    la clef publique peut décrypter ce qui a été encrypté avec la clef privée.
    Pour dire à la machine d’accepter un couple (clef privée,clef publique), il suffit
    de noter la clef publique dans le fichier .ssh/authorized_keys